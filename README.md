# OddBot - бот для Skype и Telegram

Для получения списка доступных команд отправьте боту сообщение `help`

[![Добавить бота](https://dev.botframework.com/Client/Images/Add-To-Skype-Buttons.png)](https://join.skype.com/bot/9b1c4fc1-beac-4466-8da3-b2a68544f36b)

[@OddLabsBot](https://t.me/OddLabsBot)
