const restify = require('restify');
const builder = require('botbuilder');
const debug = require('debug')('connectors:botbuilder');

const BaseConnector = require('./base');

class BotBuilderConnector extends BaseConnector {

    constructor({appId, appPassword, port}) {
        super();

        this.port = port;

        this.server = restify.createServer();

        this.connector = new builder.ChatConnector({
            appId: appId,
            appPassword: appPassword
        });

        this.server.post('/api/messages', this.connector.listen());

        const inMemoryStorage = new builder.MemoryBotStorage();

        this.bot = new builder.UniversalBot(this.connector, (session) => {
            debug('message', session.message);

            const address = Object.assign({}, session.message.address);
            delete address.id;
            let text = session.message.text;

            if (address.conversation.isGroup) {
                // в групповых чатах удаляем упоминание бота из сообщения
                const re = new RegExp('@?(' + address.bot.name + '|' + address.bot.id + ')');
                text = text.replace(re, '');
            }

            const resource = {
                nickname: address.user.id,
                name: address.user.name,
                conversationId: JSON.stringify(address),
                type: session.message.type,
                source: session.message.source,
                content: text,
                locale: session.message.textLocale
            };

            this.emit('message', resource);
        }).set('storage', inMemoryStorage);
    }

    connect() {
        return new Promise((resolve, reject) => {
            this.server.listen(this.port, () => {
                debug('%s listening to %s', this.server.name, this.server.url);
                resolve();
            });
        });
    }

    sendMessage(conversationId, text) {
        this.bot.send(
            new builder.Message()
                .text(text)
                .textFormat('plain')
                .address(JSON.parse(conversationId))
        );
    }
}

module.exports = BotBuilderConnector;
