const EventEmitter = require('events');

class BaseConnector extends EventEmitter {

    connect() {
        throw new Error('Not implemented');
    }

    sendMessage(conversationId, text) {
        throw new Error('Not implemented');
    }
}

module.exports = BaseConnector;
