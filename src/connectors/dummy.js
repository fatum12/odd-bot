const net = require('net');
const Promise = require('bluebird');
const debug = require('debug')('connectors:dummy');

const BaseConnector = require('./base');

const USER_NAME = 'user';

class DummyConnector extends BaseConnector {

    constructor({host, port}) {
        super();

        this.host = host;
        this.port = port;

        this.client = new net.Socket();

        this.client.setEncoding('utf8');
        this.client.setTimeout(0);

        this.client
            .on('data', (data) => {
                this.emit('message', {
                    nickname: USER_NAME,
                    conversationId: USER_NAME,
                    type: 'text',
                    content: data.toString()
                });
            })
            .on('error', (err) => {
                debug(err);
            })
            .on('close', () => {
                debug('Disconnected');
            });
    }

    connect() {
        return new Promise((resolve, reject) => {
            this.client.connect(this.port, this.host, () => {
                debug('Connected');
                resolve();
            });
        });
    }

    sendMessage(conversationId, text) {
        this.client.write(text);
    }
}

module.exports = DummyConnector;
