const redis = require('./services/redis');

class Command {

    static get NOT_REQUIRED() {
        return '*';
    }

    constructor() {
        this.name = '';
        this.usage = '';
        this.description = '';
        this.hidden = false;
        /**
         * @type {Bot}
         */
        this.bot = null;
    }

    /**
     * Добавляет обработчики команд
     * @param {Bot} bot
     */
    register(bot) {
        this.bot = bot;
        bot.addHandler(this.getName(), this);
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        throw new Error('Not implemented');
    }

    getName() {
        return this.name;
    }

    getUsage() {
        return this.usage;
    }

    getDescription() {
        return this.description;
    }

    setCache(key, data, ttl = 0) {
        let fullKey = this.createCacheKey(key);

        let result = redis.setAsync(fullKey, JSON.stringify(data));

        if (ttl > 0) {
            redis.expire(fullKey, ttl);
        }

        return result;
    }

    getCache(key) {
        return redis
            .getAsync(this.createCacheKey(key))
            .then((value) => {
                return JSON.parse(value);
            });
    }

    dropCache(key) {
        return redis.delAsync(this.createCacheKey(key));
    }

    createCacheKey(key) {
        return `commands:${this.getName()}:${key.toLowerCase()}`;
    }

    onlyAdmin() {
        return false;
    }
}

module.exports = Command;
