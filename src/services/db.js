const mysql = require('mysql');
const debug = require('debug')('mysql');
const Promise = require('bluebird');

const config = require('../../config');

Promise.promisifyAll(require('mysql/lib/Connection').prototype);
Promise.promisifyAll(require('mysql/lib/Pool').prototype);

const connection = mysql.createConnection({
    host: config.get('db:host'),
    user: config.get('db:username'),
    password: config.get('db:password'),
    database: config.get('db:database')
});

connection.connect((err) => {
    if (err) {
        debug(err);
        return;
    }
    debug('Connected');
});

connection.on('error', (err) => {
    debug('Database error', err);
});

module.exports = connection;
