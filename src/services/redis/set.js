const redis = require('./index');

class Set {

    constructor(key) {
        this.key = key;
    }

    getAll() {
        return redis.smembersAsync(this.key);
    }

    add(value) {
        return redis.saddAsync(this.key, value);
    }

    has(value) {
        return redis.sismemberAsync(this.key, value);
    }

    remove(value) {
        return redis.sremAsync(this.key, value);
    }

    size() {
        return redis.scardAsync(this.key);
    }

    isEmpty() {
        return this
            .size()
            .then((size) => {
                return size === 0;
            });
    }
}

module.exports = Set;
