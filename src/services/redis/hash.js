const redis = require('./index');

class Hash {

    constructor(key) {
        this.key = key;
    }

    set(key, value) {
        return redis.hsetAsync(this.key, key, value);
    }

    get(key) {
        return redis.hgetAsync(this.key, key);
    }

    getAll() {
        return redis
            .hgetallAsync(this.key)
            .then((items) => {
                if (items === null) {
                    return {};
                }
                return items;
            });
    }

    remove(key) {
        return redis.hdelAsync(this.key, key);
    }
}

module.exports = Hash;
