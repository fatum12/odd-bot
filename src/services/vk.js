const VK = require('vksdk');

const config = require('../../config');

let vk = new VK({
    'appId': config.get('vk:appId'),
    'appSecret': config.get('vk:appSecret'),
    'language': 'ru',
    'secure': true
});

vk.setToken(config.get('vk:accessToken'));

module.exports = vk;
