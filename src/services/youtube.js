const {youtube} = require('@googleapis/youtube');
const config = require('../../config');

module.exports = youtube({
    version: 'v3',
    auth: config.get('google:searchApiKey')
});
