const request = require('request');
const Promise = require('bluebird');
const debug = require('debug')('google');

const config = require('../../config');

const API_ENDPOINT = 'https://www.googleapis.com/customsearch/v1';
const defaults = {
    filter: 1,
    num: 1,
    gl: 'ru',
    googlehost: 'google.ru',
    key: config.get('google:searchApiKey'),
    cx: config.get('google:searchId')
};

let google = {
    search(query, limit = 1) {
        let params = prepareParams(query, limit);

        return search(params);
    },
    imageSearch(query, limit = 1) {
        let params = prepareParams(query, limit);
        params.searchType = 'image';

        return search(params);
    }
};

function prepareParams(query, limit) {
    let params = Object.assign({}, defaults);
    params.q = query;
    params.num = limit;

    return params;
}

function search(params) {
    if (!('q' in params)) {
        throw new Error('Не указан поисковый запрос');
    }

    debug('Поиск по запросу', params);

    return new Promise((resolve, reject) => {
        request({
            uri: API_ENDPOINT,
            qs: params
        }, (error, response, body) => {
            if (error || response.statusCode !== 200) {
                debug(error);
                reject('Не удалось загрузить результат');
            } else {
                try {
                    resolve(JSON.parse(body));
                } catch (e) {
                    reject(e);
                }
            }
        });
    });
}

module.exports = google;
