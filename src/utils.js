const moment = require('moment');

module.exports = {
    /**
     * Возвращает случайное целое число между min (включительно) и max (не включая max)
     * Использование метода Math.round() даст вам неравномерное распределение!
     * @param {number} min
     * @param {number} max
     * @returns {number}
     */
    randomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    },
    inArray(arr, item) {
        return arr.indexOf(item) != -1;
    },
    removeFromArray(arr, item) {
        let index = arr.indexOf(item);
        if (index > -1) {
            arr.splice(index, 1);
        }
        return arr;
    },
    timestamp() {
        return Math.floor(Date.now() / 1000);
    },
    isTime(time) {
        return /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/.test(time);
    },
    /**
     * Функция проверяет, что указанный момент времени находится в будущем
     * @param {string} time ЧЧ:ММ
     * @returns {boolean}
     */
    isFuture(time) {
        let t = moment(time, 'HH:mm');
        return t.isAfter();
    },
    /**
     * Возвращает время, оставшееся до момента time, в человекочитаемом формате
     * @param {string} time ЧЧ:ММ
     * @returns {string}
     */
    timeLeft(time) {
        let t = moment(time, 'HH:mm'),
            now = moment(),
            diff = moment.duration(t.diff(now));

        // округляем до минут
        if (diff.seconds() > 0) {
            diff.add(1, 'm');
        }
        let hours = diff.hours(),
            minutes = diff.minutes(),
            output = '';

        if (hours > 0) {
            output += hours + ' ч ';
        }
        if (minutes > 0) {
            output += minutes + ' мин';
        }

        return output;
    },
    formatBytes(bytes, decimals = 2) {
        if (!bytes) {
            return '0 Bytes';
        }
        let k = 1000,
            sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(decimals)) + ' ' + sizes[i];
    },
    percentage(total, part, decimals = 0) {
        return (part / total * 100).toFixed(decimals) + '%';
    }
};
