const Command = require('./command');
const utils = require('./utils');
const RedisSet = require('./services/redis/set');
const debug = require('debug')('periodic');

const TIMEOUT = 10 * 60 * 1000;   // 10 минут
const START_COMMAND = 'start';
const STOP_COMMAND = 'stop';

class PeriodicCommand extends Command {

    constructor() {
        super();

        this.timer = null;
        this.lastUpdate = null;
        this.timeout = TIMEOUT;
    }

    init() {
        this.usage = `${this.getName()} [${START_COMMAND} ИЛИ ${STOP_COMMAND}]`;
        this.subscriptions = new RedisSet(`commands:${this.getName()}:subscriptions`);

        this.subscriptions
            .isEmpty()
            .then((isEmpty) => {
                if (!isEmpty) {
                    this.startTimer();
                }
            });
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        let command = message.getText().toLowerCase();

        if (command === START_COMMAND) {
            this.start(message.getConversationId());
        } else if (command === STOP_COMMAND) {
            this.stop(message.getConversationId());
        } else {
            this.showHelp(message.getConversationId());
        }
    }

    start(conversationId) {
        this.subscriptions
            .has(conversationId)
            .then((has) => {
                if (has) {
                    this.bot.sendMessage(conversationId, `${this.getName()}: отслеживание уже запущено`);
                } else {
                    this.subscriptions
                        .add(conversationId)
                        .then(() => {
                            this.bot.sendMessage(conversationId, `${this.getName()}: начинаю отслеживание`);
                            return this.subscriptions.size();
                        })
                        .then((size) => {
                            if (size == 1) {
                                this.startTimer();
                            }
                        });
                }
            });
    }

    stop(conversationId) {
        this.subscriptions
            .has(conversationId)
            .then((has) => {
                if (!has) {
                    this.bot.sendMessage(conversationId, `${this.getName()}: отслеживание не запущено`);
                } else {
                    this.subscriptions
                        .remove(conversationId)
                        .then(() => {
                            this.bot.sendMessage(conversationId, `${this.getName()}: отслеживание остановлено`);
                            return this.subscriptions.isEmpty();
                        })
                        .then((isEmpty) => {
                            if (isEmpty) {
                                this.stopTimer();
                            }
                        });
                }
            });
    }

    showHelp(conversationId) {
        let text = '';

        this.subscriptions
            .has(conversationId)
            .then((has) => {
                if (has) {
                    text += 'Отслеживание запущено';
                } else {
                    text += 'Отслеживание не запущено';
                }

                text += '\n' + this.getName() + ' ' + START_COMMAND + ' - начать отслеживание\n' +
                    this.getName() + ' ' + STOP_COMMAND + ' - закончить отслеживание';
                this.bot.sendMessage(conversationId, text);
            });
    }

    startTimer() {
        this.lastUpdate = utils.timestamp();
        this.stopTimer();
        debug('start timer');

        this.timer = setInterval(() => {
            this.checkForUpdates();
        }, this.timeout);
    }

    stopTimer() {
        debug('stop timer');
        clearInterval(this.timer);
    }

    checkForUpdates() {
        throw new Error('Not implemented');
    }

    isStarted(conversationId) {
        return this.subscriptions.has(conversationId);
    }
}

module.exports = PeriodicCommand;
