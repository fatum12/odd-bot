const config = require('../config');
const utils = require('./utils');

class Message {

    /**
     * @param {Object} resource
     * @param {Bot} bot
     */
    constructor(resource, bot) {
        this.resource = resource;
        this.bot = bot;
    }

    getType() {
        return this.resource.type;
    }

    getUsername() {
        return this.resource.nickname;
    }

    getName() {
        return this.resource.name || this.getUsername();
    }

    getConversationId() {
        return this.resource.conversationId;
    }

    /**
     * @return {string}
     */
    getSource() {
        return this.resource.source || '';
    }

    /**
     * @return {string}
     */
    getText() {
        return this.resource.content;
    }

    setText(text) {
        this.resource.content = text;
    }

    getLocale() {
        return this.resource.locale || 'ru';
    }

    reply(/* args */) {
        let args = Array.prototype.slice.call(arguments, 0);
        args.unshift(this.getConversationId());
        this.bot.sendMessage.apply(this.bot, args);
    }

    isAdminUser() {
        return utils.inArray(config.get('administrators'), this.getUsername());
    }
}

module.exports = Message;
