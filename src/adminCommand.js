const Command = require('./command');

class AdminCommand extends Command {
    onlyAdmin() {
        return true;
    }
}

module.exports = AdminCommand;
