const Command = require('../command');

class Ping extends Command {

    constructor() {
        super();

        this.name = 'ping';
        this.usage = this.name;
        this.description = 'Проверка доступности бота';
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        message.reply('pong');
    }
}

module.exports = Ping;
