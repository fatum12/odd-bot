const Command = require('../command');
const moment = require('moment');
const debug = require('debug')('commands:countdown');
const utils = require('../utils');
const RedisHash = require('../services/redis/hash');
const _ = require('lodash');

const STOP_COMMAND = 'стоп';

class Countdown extends Command {

    constructor() {
        super();

        this.name = 'отсчет';
        this.usage = `${this.name} [ЧЧ:ММ ИЛИ ${STOP_COMMAND}]`;
        this.description = 'Отсчет оставшегося времени до указанного момента';

        this.times = {};
        this.timers = {};
        this.storage = new RedisHash(`commands:${this.getName()}:timers`);

        // восстанавливаем состояние таймеров
        this.storage
            .getAll()
            .then((times) => {
                _.forOwn(times, (time, conversationId) => {
                    if (utils.isFuture(time)) {
                        this.start(conversationId, time, true);
                    } else {
                        this.storage.remove(conversationId);
                    }
                });
            });
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        let text = message.getText().toLowerCase();
        let conversationId = message.getConversationId();

        if (utils.isTime(text)) {
            this.start(conversationId, text);
        } else if (text === STOP_COMMAND) {
            this.stop(conversationId);
        } else if (conversationId in this.times) {
            this.showNotification(conversationId, this.times[conversationId]);
        } else {
            message.reply('Необходимо указать время в формате ЧЧ:ММ');
        }
    }

    start(conversationId, time, silent = false) {
        if (conversationId in this.times) {
            this.bot.sendMessage(conversationId, 'Отсчет уже запущен');
            return;
        }

        if (!utils.isFuture(time)) {
            this.bot.sendMessage(conversationId, 'Указанный момент времени уже прошел');
            return;
        }
        this.times[conversationId] = time;
        this.notify(conversationId, time, silent);
        this.storage.set(conversationId, time);
    }

    stop(conversationId) {
        if (!(conversationId in this.times)) {
            this.bot.sendMessage(conversationId, 'Отсчет не запущен');
            return;
        }

        this.clearTimer(conversationId);
        this.bot.sendMessage(conversationId, 'Отсчет остановлен');
    }

    notify(conversationId, time, silent = false) {
        let t = moment(time, 'HH:mm'),
            now = moment(),
            diff = moment.duration(t.diff(now)),
            milliseconds = diff.asMilliseconds();

        // погрешность 1 сек
        if (milliseconds > 1000) {
            if (!silent) {
                this.showNotification(conversationId, time);
            }

            let next;
            // показываем уведомление не чаще 1 раза в 5 минут
            if (diff.asMinutes() < 10) {
                next = milliseconds;
            } else {
                next = Math.floor(milliseconds / 2);
            }
            
            this.timers[conversationId] = setTimeout(() => {
                this.notify(conversationId, time);
            }, next);
        } else {
            this.bot.sendMessage(conversationId, `${time} - Время вышло`);
            this.clearTimer(conversationId);
        }
    }

    showNotification(conversationId, time) {
        this.bot.sendMessage(conversationId, `Осталось ${utils.timeLeft(time)}`);
    }

    clearTimer(conversationId) {
        delete this.times[conversationId];
        if (conversationId in this.timers) {
            clearTimeout(this.timers[conversationId]);
            delete this.timers[conversationId];
        }
        this.storage.remove(conversationId);
    }
}

module.exports = Countdown;
