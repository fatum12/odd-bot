const Command = require('../command');
const weather = require('openweathermap');
const debug = require('debug')('commands:weather');

const CACHE_TIME = 0.5 * 60 * 60;     // 30 минут

weather.defaults({
    units: 'metric',
    lang: 'ru'
});

class Weather extends Command {
    
    constructor(apiKey) {
        super();

        this.name = 'погода';
        this.usage = this.name + ' [город]';
        this.description = 'Получить прогноз погоды для указанного города';
        this.apiKey = apiKey;
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        let cityName = message.getText();

        if (!cityName.length) {
            message.reply('Укажите название города');
            return;
        }

        this
            .getCache(cityName)
            .then((savedResult) => {
                if (savedResult !== null) {
                    this.sendResult(message, savedResult);
                } else {
                    debug(`Загрузка погоды для города: ${cityName}`);
                    weather.now({
                        q: encodeURIComponent(message.getText()),
                        APPID: this.apiKey
                    }, (err, result) => {
                        if (err) {
                            debug(err);
                            this.sendResult(message, null);
                        } else {
                            debug(result);
                            this.setCache(message.getText(), result, CACHE_TIME);
                            this.sendResult(message, result);
                        }
                    });
                }
            });
    }

    /**
     * @param {Message} message
     * @param {Object} result
     */
    sendResult(message, result) {
        if (!result) {
            message.reply('Не удалось получить прогноз погоды');
        } else {
            if ('cod' in result && result.cod === 200) {
                message.reply(`Погода для ${result.name}, ${result.sys.country}: ` +
                    `${result.main.temp}°, ${result.weather[0].description}`);
            } else {
                message.reply(`Город "${message.getText()}" не найден`);
            }
        }
    }
}

module.exports = Weather;
