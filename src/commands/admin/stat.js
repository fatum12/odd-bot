const os = require('os');
const AdminCommand = require('../../adminCommand');
const utils = require('../../utils');
const moment = require('moment');

class Stat extends AdminCommand {

    constructor() {
        super();

        this.name = 'stat';
        this.usage = this.name;
        this.description = 'Показать системную информацию';
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        let result = [];

        let la = os.loadavg().map((laItem) => {
            return laItem.toFixed(2);
        });
        result.push(`Load average: ${la.join(' ')}`);

        let freemem = os.freemem();
        let totalmem = os.totalmem();
        let usedmem = totalmem - freemem;
        result.push(`Memory: ${utils.formatBytes(usedmem, 2)} / ${utils.formatBytes(totalmem, 2)} [${utils.percentage(totalmem, usedmem)}]`);

        let uptime = moment.duration(parseInt(os.uptime()), 'seconds');
        result.push(`Uptime: ${uptime.humanize()}`);

        message.reply(result.join('\n'));
    }
}

module.exports = Stat;
