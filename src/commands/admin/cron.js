const CronJob = require('cron').CronJob;

const AdminCommand = require('../../adminCommand');
const Message = require('../../message');
const RedisSet = require('../../services/redis/set');
const debug = require('debug')('commands:cron');

const STOP_COMMAND = 'stop';
const LIST_COMMAND = 'list';

class Cron extends AdminCommand {

    constructor() {
        super();

        this.name = 'cron';
        this.usage = this.name;
        this.description = 'Добавить периодическую задачу';

        this.conversations = new RedisSet(`commands:${this.getName()}:conversations`);
        this.jobs = {};

        // восстанавливаем состояние задач после перезапуска
        this.conversations.getAll()
            .then((conversations) => {
                conversations.forEach((conversationId) => {
                    let key = this.createJobsKey(conversationId);

                    this.getCache(key)
                        .then((jobs) => {
                            jobs.forEach(([pattern, command]) => {
                                this.createJob(conversationId, pattern, command);
                            });
                        });
                });
            })
        ;
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        const text = message.getText();
        const parts = text.split(' ');

        if (parts[0] === STOP_COMMAND) {
            if (parts.length === 1) {
                this.stop(message.getConversationId());
                message.reply('Все задачи остановлены');
            } else {
                this.stopByIndex(message.getConversationId(), parseInt(parts[1]) - 1);
            }
        } else if (parts[0] === LIST_COMMAND) {
            this.list(message.getConversationId());
        } else {
            if (parts.length < 6) {
                this.showHelp(message.getConversationId());
                return;
            }
            // в команде нужно заменять * на # чтобы не было конфликта с форматированием скайпа, здесь проводим обратное преобразование
            const periodPattern = parts.slice(0, 6).join(' ').replace(/#/g, '*');
            let command = parts.slice(6).join(' ');

            try {
                this.createJob(message.getConversationId(), periodPattern, command);
            } catch (e) {
                message.reply(e.message);
                return;
            }
            this.saveJob(message.getConversationId(), periodPattern, command);
            message.reply('Задача добавлена');
        }
    }

    createJob(conversationId, pattern, command) {
        debug('pattern =', pattern, '; command =', command);

        let job;

        try {
            job = new CronJob(
                pattern,
                () => {
                    const message = new Message({
                        nickname: 'user',
                        conversationId: conversationId,
                        type: 'text',
                        content: command
                    }, this.bot);

                    this.bot.handleMessage(message);
                },
                null,
                false,
                'Europe/Moscow'
            );
        } catch (ex) {
            throw new SyntaxError('Неверный паттерн: ' + ex.message);
        }

        job.start();

        if (!(conversationId in this.jobs)) {
            this.jobs[conversationId] = [];
        }
        this.jobs[conversationId].push(job);
    }

    saveJob(conversationId, pattern, command) {
        this.conversations.add(conversationId);
        const key = this.createJobsKey(conversationId);

        this.getCache(key)
            .then((jobs) => {
                if (jobs === null) {
                    jobs = [];
                }
                jobs.push([pattern, command]);
                return this.setCache(key, jobs);
            })
        ;
    }

    /**
     * Останавливает все задачи
     * @param conversationId
     */
    stop(conversationId) {
        if (!(conversationId in this.jobs)) {
            return;
        }
        this.jobs[conversationId].forEach((job) => {
            job.stop();
        });
        delete this.jobs[conversationId];

        this.dropCache(this.createJobsKey(conversationId));
        this.conversations.remove(conversationId);
    }

    /**
     * Останавливает задачу с указанным номером
     * @param conversationId
     * @param index
     */
    stopByIndex(conversationId, index) {
        if (!(conversationId in this.jobs)) {
            return;
        }

        let jobs = this.jobs[conversationId];
        if (index < 0 || index >= jobs.length) {
            this.bot.sendMessage(conversationId, 'Задача с таким номером не найдена');
            return;
        }

        this.bot.sendMessage(conversationId, 'Задача остановлена');

        if (jobs.length === 1) {
            // в списке всего одна задача
            this.stop(conversationId);
            return;
        }

        jobs[index].stop();
        jobs.splice(index, 1);

        const key = this.createJobsKey(conversationId);
        this.getCache(key)
            .then(cachedJobs => {
                cachedJobs.splice(index, 1);
                return this.setCache(key, cachedJobs);
            })
        ;
    }

    /**
     * Выводит список всех задач
     * @param conversationId
     */
    list(conversationId) {
        if (!(conversationId in this.jobs)) {
            this.bot.sendMessage(conversationId, 'Список пуст');
            return;
        }
        this.getCache(this.createJobsKey(conversationId))
            .then((jobs) => {
                let parts = [];
                let index = 0;
                jobs.forEach(([pattern, command]) => {
                    parts.push(`${++index}.\t${pattern}\t${command}`);
                });
                this.bot.sendMessage(conversationId, parts.join('\n'));
            })
        ;
    }

    createJobsKey(conversationId) {
        return `commands:${this.getName()}:jobs:${conversationId}`;
    }

    showHelp(conversationId) {
        const msg = `${this.name} [second] [minute] [hour] [day] [month] [weekday] [command] - добавить задачу\n` +
            `${this.name} ${STOP_COMMAND} - остановить все задачи\n` +
            `${this.name} ${STOP_COMMAND} [index] - остановить задачу с указанным номером\n` +
            `${this.name} ${LIST_COMMAND} - показать список активных задач`;

        this.bot.sendMessage(conversationId, msg);
    }
}

module.exports = Cron;
