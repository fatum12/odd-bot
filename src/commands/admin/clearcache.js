const AdminCommand = require('../../adminCommand');
const redis = require('../../services/redis');

class ClearCache extends AdminCommand {

    constructor() {
        super();

        this.name = 'clearcache';
        this.usage = this.name;
        this.description = 'Очистить весь кеш';
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        redis
            .flushdbAsync()
            .then(() => {
                message.reply('Кеш очищен');
            });
    }
}

module.exports = ClearCache;
