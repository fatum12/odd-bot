const Command = require('../command');
const PeriodicCommand = require('../periodicCommand');
const _ = require('lodash');
const utils = require('../utils');
const moment = require('moment');
const RedisHash = require('../services/redis/hash');

class StopAll extends Command {
    
    constructor() {
        super();

        this.name = 'stopall';
        this.usage = `${this.name} [ЧЧ:ММ]${Command.NOT_REQUIRED}`;
        this.description = 'Остановить все отслеживания';

        this.timers = {};
        this.storage = new RedisHash(`commands:${this.getName()}:timers`);

        // восстанавливаем состояние таймеров
        this.storage
            .getAll()
            .then((times) => {
                _.forOwn(times, (time, conversationId) => {
                    if (utils.isFuture(time)) {
                        this.delayedStop(conversationId, time, true);
                    } else {
                        this.storage.remove(conversationId);
                    }
                });
            });
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        const text = message.getText();
        const conversationId = message.getConversationId();

        if (utils.isTime(text)) {
            this.delayedStop(conversationId, text);
        } else {
            this.stop(conversationId);
        }
    }

    stop(conversationId) {
        const periodicCommands = _.filter(this.bot.getHandlers(), command => {
            return command instanceof PeriodicCommand;
        });

        periodicCommands.forEach(command => {
            command
                .isStarted(conversationId)
                .then(started => {
                    if (started) {
                        command.stop(conversationId);
                    }
                });
        });
        this.clearTimer(conversationId);
    }

    delayedStop(conversationId, time, silent = false) {
        if (!utils.isFuture(time)) {
            this.bot.sendMessage(conversationId, 'Указанный момент времени уже прошел');
            return;
        }
        this.clearTimer(conversationId);
        let t = moment(time, 'HH:mm'),
            now = moment(),
            diff = moment.duration(t.diff(now));

        this.timers[conversationId] = setTimeout(() => {
            this.stop(conversationId);
        }, diff.asMilliseconds());
        this.storage.set(conversationId, time);

        if (!silent) {
            this.bot.sendMessage(conversationId, `Отслеживание будет остановлено через ${utils.timeLeft(time)}`);
        }
    }

    clearTimer(conversationId) {
        if (conversationId in this.timers) {
            clearTimeout(this.timers[conversationId]);
            delete this.timers[conversationId];
        }
        this.storage.remove(conversationId);
    }
}

module.exports = StopAll;
