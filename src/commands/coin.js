const Command = require('../command');
const utils = require('../utils');

const SIDES = ['орел', 'решка'];

class Coin extends Command {

    constructor() {
        super();

        this.name = 'монета';
        this.usage = this.name;
        this.description = 'Бросает монетку и возвращает "орел" или "решка"';
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        message.reply(SIDES[utils.randomInt(0, 2)]);
    }
}

module.exports = Coin;
