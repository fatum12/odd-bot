const Command = require('../command');
const request = require('request');
const Promise = require('bluebird');
const debug = require('debug')('commands:advice');
const htmlToText = require('html-to-text');

const URL = 'http://fucking-great-advice.ru/api/random';

class Advice extends Command {

    constructor() {
        super();

        this.name = 'совет';
        this.usage = this.name;
        this.description = 'Получить полезный совет';
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        this
            .load()
            .then(
                (result) => {
                    message.reply(result);
                },
                () => {
                    message.reply('Не удалось загрузить совет. Попробуйте повторить позднее.')
                }
            );
    }

    // Загружает совет
    load() {
        debug('Загрузка совета');

        return new Promise((resolve, reject) => {
            request(URL, (error, response, body) => {
                if (error || response.statusCode !== 200) {
                    debug(error);
                    reject();
                } else {
                    try {
                        let result = JSON.parse(body);
                        debug(result);
                        resolve(htmlToText.fromString(result.text, {
                            wordwrap: null
                        }));
                    } catch (e) {
                        debug(e);
                        reject();
                    }
                }
            });
        });
    }
}

module.exports = Advice;
