const Command = require('../command');
const math = require('mathjs');

class Calc extends Command {

    constructor() {
        super();

        this.name = '=';
        this.usage = `${this.name} [математическое выражение]`;
        this.description = 'Калькулятор'
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        let text = message.getText();

        if (text === '') {
            // пустой запрос
            message.reply('Укажите математическое выражение');
            return;
        }

        let result;
        try {
            result = math.eval(text);
        } catch (e) {
            result = 'Ошибка: ' + e.message;
        }
        message.reply(result);
    }
}

module.exports = Calc;
