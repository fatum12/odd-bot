const Command = require('../command');
const debug = require('debug')('commands:google');
const google = require('../services/google');

const CACHE_TIME = 2 * 60 * 60;   // 2 часа

class Google extends Command {

    constructor() {
        super();

        this.name = 'google';
        this.usage = `${this.name} [запрос]`;
        this.description = 'Поиск в Google';
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        let query = message.getText().toLowerCase();

        if (!query.length) {
            message.reply('Введите поисковый запрос');
            return;
        }
        this
            .getCache(query)
            .then((result) => {
                if (result !== null) {
                    return result;
                }
                return this
                    .search(query)
                    .then((result) => {
                        this.setCache(query, result, CACHE_TIME);
                        return result;
                    });
            })
            .then(
                (result) => {
                    let text = result.title;
                    text += '\n' + result.snippet.replace(/\n/g, '');
                    text += '\n' + result.link;


                    message.reply(text);
                },
                (error) => {
                    debug(error);
                    message.reply('Не удалось выполнить запрос. Попробуйте повторить позднее.');
                }
            );
    }

    /**
     * Поиск в Google
     * @param {string} query Поисковый запрос
     */
    search(query) {
        return google
            .search(query, 1)
            .then((result) => {
                return result.items.pop();
            });
    }
}

module.exports = Google;
