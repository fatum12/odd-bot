const Command = require('../command');
const whois = require('whois');
const debug = require('debug')('commands:whois');

class Whois extends Command {
    
    constructor() {
        super();

        this.name = 'whois';
        this.usage = `${this.name} [домен ИЛИ ip]`;
        this.description = 'Показать информацию о доменном имени или ip-адресе';
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        let query = message.getText().toLowerCase();

        if (!query.length) {
            message.reply('Укажите доменное имя или ip-адрес');
            return;
        }
        whois.lookup(query, (err, data) => {
            if (err) {
                debug(err);
                message.reply('Не удалось выполнить запрос. Попробуйте повторить позднее.');
            } else {
                message.reply(data.trim());
            }
        });
    }
}

module.exports = Whois;
