const Command = require('../command');
const request = require('request');
const cheerio = require('cheerio');
const Promise = require('bluebird');
const htmlToText = require('html-to-text');
const debug = require('debug')('commands:bash');

const URL = 'http://bash.im/random';
const CACHE_TIME = 60 * 60;   // 1 час
const CACHE_KEY = 'quotes';

class Bash extends Command {

    constructor() {
        super();

        this.name = 'bash';
        this.usage = this.name;
        this.description = 'Показать случайную цитату с сайта bash.im';
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        this.getOneQuote()
            .then(
                (quote) => {
                    message.reply(quote);
                },
                () => {
                    message.reply('Не удалось загрузить цитату. Попробуйте повторить позднее.');
                }
            );
    }

    getOneQuote() {
        let result;

        return this
            .getCache(CACHE_KEY)
            .then((quotes) => {
                if (quotes === null || !quotes.length) {
                    return loadQuotes();
                } else {
                    return quotes;
                }
            })
            .then((quotes) => {
                result = quotes.pop();
                this.setCache('quotes', quotes, CACHE_TIME);
                return result;
            });
    }
}

function loadQuotes() {
    debug('Загрузка цитат');

    return new Promise((resolve, reject) => {
        request({
            url: URL,
            encoding: null
        }, (error, response, body) => {
            if (error || response.statusCode !== 200) {
                debug(error);
                reject();
            }

            let $ = cheerio.load(body, {
                    decodeEntities: false
                }),
                quotes = [];

            $('.quote__body').each(function (i, elem) {
                let html = $(this).html();
                quotes.push(htmlToText.fromString(html, {
                    wordwrap: null
                }));
            });
            debug(`Загружено ${quotes.length} цитат`);

            if (quotes.length) {
                resolve(quotes);
            } else {
                reject();
            }
        });
    });
}

module.exports = Bash;
