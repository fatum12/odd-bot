const request = require('request');
const debug = require('debug')('commands:gif');

const Command = require('../command');
const config = require('../../config');

const API_ENDPOINT = 'https://g.tenor.com/v1/search';

    class Gif extends Command {

    constructor() {
        super();

        this.name = 'gif';
        this.usage = `${this.name} [запрос]`;
        this.description = 'Поиск гифок';
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        const query = message.getText();
        if (!query.length) {
            message.reply('Введите поисковый запрос');
            return;
        }

        const timeout = config.get('tenor:timeout') || 5;
        const options = {
            uri: API_ENDPOINT,
            timeout: timeout * 1000,
            qs: {
                key: config.get('tenor:apiKey'),
                q: query,
                locale: message.getLocale(),
                contentfilter: 'off',
                media_filter: 'minimal',
                ar_range: 'all',
                limit: 1
            },
            json: true
        };
        debug(options);
        request(options, (error, response, body) => {
            if (error) {
                debug(error);
                message.reply('Не удалось выполнить запрос: ' + error);
                return;
            }
            const results = body.results;
            if (!results.length) {
                message.reply('Ничего не найдено');
                return;
            }
            const url = results[0].media[0].gif.url;
            message.reply(url);
        });
    }
}

module.exports = Gif;
