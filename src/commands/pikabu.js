const PeriodicCommand = require('../periodicCommand');
const utils = require('../utils');
const Promise = require('bluebird');
const debug = require('debug')('commands:pikabu');
const _ = require('lodash');
const vk = require('../services/vk');

const TIMEOUT = 15 * 60 * 1000;   // 15 минут
const VK_GROUP_ID = -31480508;

class Pikabu extends PeriodicCommand {
    
    constructor() {
        super();

        this.name = 'pikabu';
        this.description = 'Отслеживание постов на сайте pikabu.ru';

        this.timeout = TIMEOUT;
        this.init();
    }

    loadPosts() {
        debug('Загрузка постов');

        return new Promise((resolve, reject) => {
            vk.request(
                'wall.get',
                {
                    owner_id: VK_GROUP_ID,
                    count: 10,
                    filter: 'owner'
                },
                (res) => {
                    debug(res);

                    if ('response' in res && 'items' in res.response) {
                        resolve(res.response.items);
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    checkForUpdates() {
        this
            .loadPosts()
            .then((articles) => {
                let sorted = _.sortBy(articles, (a) => {
                    return a.date;
                });
                debug(this.lastUpdate);
                debug(sorted);
                let fromIndex = _.findIndex(sorted, (a) => {
                    return a.date > this.lastUpdate;
                });
                if (fromIndex > -1) {
                    let result = [];
                    for (let i = fromIndex; i < sorted.length; i++) {
                        result.push(this.makePreview(sorted[i]));
                    }
                    result = result.join('\n\n');
                    // отсылаем обновление
                    this.subscriptions
                        .getAll()
                        .then((subscriptions) => {
                            subscriptions.forEach((conversationId) => {
                                this.bot.sendMessage(conversationId, result);
                            });
                        });
                }
                this.lastUpdate = utils.timestamp();
            });
    }

    /**
     * @param {Object} post
     * @return {string}
     */
    makePreview(post) {
        let text = '';

        if ('attachments' in post) {
            // пробуем найти ссылку в attachments
            let linkObj = _.find(post.attachments, (attachment) => {
                return attachment.type === 'link';
            });
            if (linkObj) {
                text = linkObj.link.title + '\n' + linkObj.link.url;
            }
        }
        if (text === '') {
            // извлекаем данные из текста поста
            text = post.text.split('\n')[0];
            let matches = post.text.match(/Комментарии:\s(.+)/);
            if (matches) {
                text += '\n' + 'http://' + matches[1];
            }
        }

        return text;
    }
}

module.exports = Pikabu;
