const Command = require('../command');
const debug = require('debug')('commands:youtube');
const entities = require('entities');
const youtubeClient = require('../services/youtube');

class YouTube extends Command {

    constructor() {
        super();

        this.name = 'yt';
        this.usage = `${this.name} [запрос]`;
        this.description = 'Поиск по YouTube';
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        const query = message.getText();
        if (!query.length) {
            message.reply('Введите поисковый запрос');
            return;
        }

        youtubeClient.search
            .list({
                part: 'id,snippet',
                q: query,
                maxResults: 1,
                type: 'video',
                videoDimension: '2d'
            })
            .then(res => {
                debug(res.data);

                const items = res.data.items;
                if (!items.length) {
                    message.reply('Ничего не найдено');
                    return;
                }

                const video = items[0];
                debug(video);
                const title = entities.decodeHTML(video.snippet.title);
                const url = 'https://www.youtube.com/watch?v=' + video.id.videoId;
                message.reply(`${title}\n${url}`)
            })
            .catch(error => {
                debug(error);
                message.reply('Не удалось выполнить запрос: ' + error);
            });
    }
}

module.exports = YouTube;
