const Command = require('../command');
const crypto  = require('crypto');

const CHARS = '23456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
const DEFAULT_LENGTH = 8;
const MAX_LENGTH = 256;

class Password extends Command {
    
    constructor() {
        super();

        this.name = 'пароль';
        this.usage = this.name + ' [количество символов]' + Command.NOT_REQUIRED;
        this.description = 'Сгенерировать случайный пароль указанной длины';
    }

    handleMessage(message) {
        let length = parseInt(message.getText()) || DEFAULT_LENGTH;
        if (length > MAX_LENGTH) {
            message.reply(`Не более ${MAX_LENGTH} символов`);
            return;
        }
        message.reply(generatePassword(length));
    }
}

function generatePassword(length) {
    let bf = crypto.randomBytes(length),
        result = '',
        charsLength = CHARS.length;

    for (let i = 0; i < bf.length; i++) {
        let index = bf.readUInt8(i) % charsLength;
        result += CHARS.charAt(index);
    }

    return result;
}

module.exports = Password;
