const Command = require('../command');
const request = require('request');
const parseString = require('xml2js').parseString;
const Promise = require('bluebird');
const iconv = require('iconv-lite');
const debug = require('debug')('commands:currency');

const API_ENDPOINT = 'http://www.cbr.ru/scripts/XML_daily.asp';
const CACHE_TIME = 4 * 60 * 60;   // 4 часа

class Currency extends Command {

    constructor() {
        super();

        this.name = 'курс';
        this.usage = `${this.name} [код валюты]${Command.NOT_REQUIRED}`;
        this.description = 'Показать курс валюты';
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        let currencyCode = message.getText().toLowerCase();

        this.getCourses()
            .then(
                (courses) => {
                    if (!currencyCode.length) {
                        // пустой код валюты - отправляем курсы доллара и евро
                        let text = courses.usd.name + ': ' + courses.usd.value + ' руб.\n';
                        text += courses.eur.name + ': ' + courses.eur.value + ' руб.';

                        message.reply(text);
                    } else if (!(currencyCode in courses)) {
                        // неверный код валюты - отправляем справку
                        this.sendHelp(message);
                    } else {
                        message.reply(`${courses[currencyCode].name}: ${courses[currencyCode].value} руб.`);
                    }
                },
                () => {
                    message.reply('Не удалось загрузить курсы валют');
                }
            );
    }

    getCourses() {
        return this
            .getCache('courses')
            .then((courses) => {
                if (courses !== null) {
                    return courses;
                } else {
                    return this.loadCourses();
                }
            });
    }

    loadCourses() {
        debug('Загрузка курсов валют');

        return new Promise((resolve, reject) => {
            request({
                url: API_ENDPOINT,
                encoding: null
            }, (error, response, body) => {
                if (error || response.statusCode !== 200) {
                    debug(error);
                    reject();
                } else {
                    // преобразуем в utf8
                    body = iconv.decode(body, 'win1251');

                    parseString(body, {explicitArray: false}, (err, result) => {
                        if (err) {
                            debug(err);
                            reject();
                        } else {
                            let currencies = {};
                            result.ValCurs.Valute.forEach((valute) => {
                                currencies[valute.CharCode.toLowerCase()] = {
                                    name: valute.Name,
                                    value: valute.Value
                                };
                            });
                            this.setCache('courses', currencies, CACHE_TIME);
                            debug(currencies);
                            resolve(currencies);
                        }
                    });
                }
            });
        });
    }

    /**
     * @param {Message} message
     */
    sendHelp(message) {
        let text = 'Неверный код валюты. Возможные варианты:';

        this.getCourses()
            .then((courses) => {
                for (let code in courses) {
                    text += '\n' + code + ' - ' + courses[code].name;
                }

                message.reply(text);
            });
    }
}

module.exports = Currency;
