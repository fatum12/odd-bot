const Command = require('../../command');
const State = require('./state');
const utils = require('../../utils');
const db = require('../../services/db');

const ALLOW_LETTERS = 'абвгдежзиклмнопрстуфхцчшщэюя';
const EXCLUDE_LETTERS = 'ъьый';
const CITIES_TABLE = 'cities';
const SCORE_STEP = 10;

const START_COMMAND = 'старт';
const STOP_COMMAND = 'стоп';
const STAT_COMMAND = 'стат';
const SKIP_COMMAND = 'пропуск';

class Cities extends Command {

    constructor() {
        super();

        this.name = 'города';
        this.usage = this.name;
        this.description = 'Игра "Города"';

        this.states = {};
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        const command = message.getText().toLowerCase();
        const conversationId = message.getConversationId();

        if (command === '') {
            this.showHelp(conversationId, true);
        } else if (command === START_COMMAND) {
            this.startGame(conversationId);
        } else if (command === STOP_COMMAND) {
            this.stopGame(conversationId);
        } else if (command === STAT_COMMAND) {
            this.showStat(conversationId);
        } else if (command === SKIP_COMMAND) {
            this.skipQuestion(conversationId);
        } else {
            this.checkAnswer(message);
        }
    }

    startGame(conversationId) {
        if (this.isGameStarted(conversationId)) {
            this.bot.sendMessage(conversationId, 'Игра уже запущена.');
            this.showQuestion(conversationId);
            return;
        }
        let state = new State();
        this.states[conversationId] = state;

        this.bot.sendMessage(conversationId, 'Начинаю игру.');
        state.setLetter(getRandomLetter());
        this.showQuestion(conversationId);
    }

    stopGame(conversationId) {
        if (!this.isGameStarted(conversationId)) {
            this.bot.sendMessage(conversationId, 'Игра не запущена.');
            this.showHelp(conversationId);
            return;
        }
        this.showStat(conversationId);
        delete this.states[conversationId];
    }

    showStat(conversationId) {
        let state = this.getGameState(conversationId);
        let score = state.getScore();
        let text = 'Результаты игры:\n';

        score.forEach((item) => {
            text += `${item[0]} - ${item[1]} очков\n`;
        });

        this.bot.sendMessage(conversationId, text);
    }

    showHelp(conversationId, includeDescription = false) {
        let help = '';

        if (includeDescription) {
            help += 'Игра для нескольких человек, в которой каждый участник в свою очередь называет реально существующий ' +
                'город любой страны, название которого начинается на ту букву, которой оканчивается название города предыдущего участника.\n\n';
        }

        let items = {
            [START_COMMAND]: 'начать игру',
            [STOP_COMMAND]: 'закончить игру',
            [STAT_COMMAND]: 'показать результаты',
            [SKIP_COMMAND]: 'пропустить букву'

        };

        for (let command in items) {
            help += `${this.name} ${command} - ${items[command]}\n`;
        }

        this.bot.sendMessage(conversationId, help);
    }

    getGameState(conversationId) {
        return this.states[conversationId];
    }

    isGameStarted(conversationId) {
        return conversationId in this.states;
    }

    showQuestion(conversationId) {
        let state = this.getGameState(conversationId);

        this.bot.sendMessage(conversationId, `Назовите город на букву "${state.getLetter().toUpperCase()}"`);
    }

    /**
     * @param {Message} message
     */
    checkAnswer(message) {
        let conversationId = message.getConversationId();
        let answer = message.getText();
        let nick = message.getName();

        if (!this.isGameStarted(conversationId)) {
            this.showHelp(conversationId);
            return;
        }
        let state = this.getGameState(conversationId);
        // заменяем букву "ё" на "е", так как в базе города хранятся без "ё"
        let city = answer.replace(/ё/g, 'е').toLowerCase();

        if (!city.startsWith(state.getLetter())) {
            // названный город начинается на другую букву
            this.showQuestion(conversationId);
            return;
        }
        if (state.hasCity(city)) {
            // город уже назывался ранее
            this.bot.sendMessage(conversationId, 'Город уже назывался');
            return;
        }
        // проверяем в базе
        db
            .queryAsync(`select 1 from ${CITIES_TABLE} WHERE name = ? limit 1`, [city])
            .then((results) => {
                if (results.length) {
                    // верный ответ
                    state.addCity(city);
                    state.incrementScore(nick, SCORE_STEP);
                    this.bot.sendMessage(conversationId, `Верно! ${nick} набирает ${state.getPlayerScore(nick)} очков`);
                    state.setLetter(determineLastLetter(city));
                    this.showQuestion(conversationId);
                } else {
                    this.bot.sendMessage(conversationId, `${city}: нет такого города`);
                }
            });
    }

    skipQuestion(conversationId) {
        let state = this.getGameState(conversationId);
        state.setLetter(getRandomLetter());
        this.showQuestion(conversationId);
    }
}

function getRandomLetter() {
    return ALLOW_LETTERS.charAt(utils.randomInt(0, ALLOW_LETTERS.length));
}

function determineLastLetter(city) {
    let lastLetter = city.charAt(city.length - 1);
    if (EXCLUDE_LETTERS.indexOf(lastLetter) !== -1) {
        lastLetter = city.charAt(city.length - 2);
    }
    return lastLetter;
}

module.exports = Cities;
