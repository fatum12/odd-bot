/**
 * Состояние игры
 */
class State {

    constructor() {
        this.cities = {};
        this.score = {};
        this.currentLetter = null;
    }

    addCity(cityName) {
        this.cities[cityName] = 1;
    }

    hasCity(cityName) {
        return cityName in this.cities;
    }

    incrementScore(player, score) {
        if (!(player in this.score)) {
            this.score[player] = 0;
        }
        this.score[player] += score;
    }

    getScore() {
        let result = [];

        for (let nick in this.score) {
            result.push([nick, this.score[nick]]);
        }
        result.sort((a, b) => {
            if (a[1] < b[1]) {
                return -1;
            }
            if (a[1] > b[1]) {
                return 1;
            }
            return 0;
        });

        return result;
    }

    getPlayerScore(player) {
        return player in this.score ? this.score[player] : 0;
    }

    getLetter() {
        return this.currentLetter;
    }

    setLetter(letter) {
        this.currentLetter = letter;
    }
}

module.exports = State;
