const Command = require('../command');
const PeriodicCommand = require('../periodicCommand');
const _ = require('lodash');

class StartAll extends Command {

    constructor() {
        super();

        this.name = 'startall';
        this.usage = this.name;
        this.description = 'Запустить все отслеживания';
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        const periodicCommands = _.filter(this.bot.getHandlers(), command => {
            return command instanceof PeriodicCommand;
        });

        periodicCommands.forEach(command => {
            command
                .isStarted(message.getConversationId())
                .then(started => {
                    if (!started) {
                        command.start(message.getConversationId());
                    }
                });
        });
    }
}

module.exports = StartAll;
