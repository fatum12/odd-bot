const Command = require('../command');
const debug = require('debug')('commands:image');
const utils = require('../utils');
const google = require('../services/google');

const CACHE_TIME = 2 * 60 * 60;   // 2 часа

class Image extends Command {

    constructor() {
        super();

        this.name = 'img';
        this.usage = this.name + ' [запрос]';
        this.description = 'Поиск картинок';
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        let query = message.getText().toLowerCase();

        if (!query.length) {
            message.reply('Введите поисковый запрос');
            return;
        }
        this
            .getCache(query)
            .then((result) => {
                if (result !== null) {
                    return result;
                }
                return this
                    .search(query)
                    .then((result) => {
                        this.setCache(query, result, CACHE_TIME);
                        return result;
                    });
            })
            .then((links) => {
                return links[utils.randomInt(0, links.length)];
            })
            .then(
                (result) => {
                    message.reply(result);
                },
                (error) => {
                    debug(error);
                    message.reply('Не удалось выполнить запрос. Попробуйте повторить позднее.');
                }
            );
    }

    /**
     * Поиск в Google Images
     * @param {string} query Поисковый запрос
     */
    search(query) {
        return google
            .imageSearch(query, 5)
            .then((result) => {
                // выбираем только ссылки
                return result.items.map((item) => {
                    return item.link;
                });
            });
    }
}

module.exports = Image;
