const Command = require('../command');
const request = require('request');
const cheerio = require('cheerio');
const Promise = require('bluebird');
const htmlToText = require('html-to-text');
const debug = require('debug')('commands:anekdot');

const URL = 'http://www.anekdot.ru/random/anekdot/';
const CACHE_TIME = 60 * 60;   // 1 час

class Anekdot extends Command {
    
    constructor() {
        super();

        this.name = 'анекдот';
        this.usage = this.name;
        this.description = 'Показать случайный анекдот с сайта anekdot.ru';
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        this
            .getOneAnecdote()
            .then(
                (anecdote) => {
                    message.reply(anecdote);
                },
                () => {
                    message.reply('Не удалось загрузить анекдот. Попробуйте повторить позднее.');
                }
            );
    }

    getOneAnecdote() {
        let result;

        return this
            .getCache('anecdotes')
            .then((anecdotes) => {
                if (anecdotes === null || !anecdotes.length) {
                    // кеша нет - загружаем анекдоты с сайта
                    return loadAnecdotes();
                } else {
                    return anecdotes;
                }
            })
            .then((anecdotes) => {
                result = anecdotes.pop();
                this.setCache('anecdotes', anecdotes, CACHE_TIME);
                return result;
            });
    }
}

function loadAnecdotes() {
    debug('Загрузка анекдотов');

    return new Promise((resolve, reject) => {
        request(URL, (error, response, body) => {
            if (error || response.statusCode !== 200) {
                debug(error);
                reject();
            } else {
                let $ = cheerio.load(body, {
                        decodeEntities: false
                    }),
                    anecdotes = [];

                $('.topicbox .text').each(function (i, elem) {
                    let html = $(this).html();
                    anecdotes.push(htmlToText.fromString(html, {
                        wordwrap: null
                    }));
                });
                debug(`Загружено ${anecdotes.length} анекдотов`);
                resolve(anecdotes);
            }
        });
    });
}

module.exports = Anekdot;
