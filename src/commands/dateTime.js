const Command = require('../command');
const moment = require('moment');

moment.locale('ru');

class DateTime extends Command {

    constructor() {
        super();

        this.name = 'дата';
        this.usage = this.name;
        this.description = 'Показать текущую дату и время';
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        message.reply(moment().format(
            'dddd, D MMMM YYYYг, HH:mm:ss (Z)[\n]' +
            'день DDD[\n]' +
            'неделя w[\n]' +
            '[timestamp] X'
        ));
    }
}

module.exports = DateTime;
