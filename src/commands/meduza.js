const PeriodicCommand = require('../periodicCommand');
const utils = require('../utils');
const request = require('request');
const Promise = require('bluebird');
const debug = require('debug')('commands:meduza');
const _ = require('lodash');

const TIMEOUT = 15 * 60 * 1000;     // 15 минут
const API_ENDPOINT = 'https://meduza.io/api/v3/search';

class Meduza extends PeriodicCommand {
    
    constructor() {
        super();

        this.name = 'meduza';
        this.description = 'Отслеживание новостей на сайте meduza.io';

        this.timeout = TIMEOUT;
        this.init();
    }

    loadArticles() {
        debug('Загрузка статей');

        return new Promise((resolve, reject) => {
            request({
                uri: API_ENDPOINT,
                qs: {
                    chrono: 'news',
                    page: 0,
                    per_page: 24,
                    locale: 'ru'
                },
                gzip: true
            }, (error, response, body) => {
                debug(body);

                if (error || response.statusCode !== 200) {
                    debug('Неверный ответ сервера: %s', response.statusCode);
                    reject();
                } else {
                    try {
                        let result = JSON.parse(body.trim());
                        resolve(result.documents);
                    } catch (e) {
                        debug(e);
                        reject();
                    }
                }
            });
        });
    }

    checkForUpdates() {
        this
            .loadArticles()
            .then((articles) => {
                const previousLastUpdate = this.lastUpdate;
                this.lastUpdate = utils.timestamp();

                let newArticles = _.filter(articles, (a) => {
                    return previousLastUpdate - TIMEOUT / 1000 <= a.published_at && a.published_at < previousLastUpdate;
                });
                debug(newArticles);

                if (newArticles.length) {
                    // появились новые статьи
                    newArticles = _.sortBy(newArticles, (a) => {
                        return a.published_at;
                    });
                    _.reverse(newArticles);
                    let result = [];
                    newArticles.forEach((article) => {
                        result.push(this.makePreview(article));
                    });
                    result = result.join('\n\n');

                    // отсылаем обновление
                    this.subscriptions
                        .getAll()
                        .then((subscriptions) => {
                            subscriptions.forEach((conversationId) => {
                                this.bot.sendMessage(conversationId, result);
                            });
                        });
                }
            });
    };

    makePreview(article) {
        let text = article.title;
        if ('second_title' in article && article.second_title.length) {
            text += '\n' + article.second_title;
        }
        text += '\n' + 'https://meduza.io/' + article.url;

        return text;
    }
}

module.exports = Meduza;
