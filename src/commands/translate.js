const Command = require('../command');
const request = require('request');
const debug = require('debug')('commands:translate');
const Promise = require('bluebird');

const GET_LANGUAGES_URL = 'https://translate.yandex.net/api/v1.5/tr.json/getLangs';
const TRANSLATE_URL = 'https://translate.yandex.net/api/v1.5/tr.json/translate';

class Translate extends Command {
    
    constructor(apiKey) {
        super();

        this.name = 'перевод';
        this.usage = `${this.name} [c какого языка] [на какой язык] [текст]`;
        this.description = 'Перевести текст с помощью Яндекс.Переводчика. Например, ' + this.name + ' ru en Привет';

        this.apiKey = apiKey;
        this.langs = {};

        this.loadLanguages();
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        let query = message.getText(),
            params = /^(\w+) (\w+) (.+)$/i.exec(query);

        if (params === null) {
            message.reply(`Укажите все параметры: ${this.usage}`);
            return;
        }
        if (!(params[1] in this.langs)) {
            message.reply('Неверный исходный язык: %s\n%s', params[1], this.getHelpText());
            return;
        }
        if (!(params[2] in this.langs)) {
            message.reply('Неверный результирующий язык: %s\n%s', params[2], this.getHelpText());
            return;
        }
        this
            .translate(params[1], params[2], params[3])
            .then(
                (result) => {
                    message.reply(result);
                },
                () => {
                    message.reply('Не удалось выполнить перевод. Попробуйте повторить позднее.');
                }
            );
    }

    loadLanguages() {
        debug('Загрузка списка языков');

        request({
            uri: GET_LANGUAGES_URL,
            qs: {
                key: this.apiKey,
                ui: 'ru'
            }
        }, (error, response, body) => {
            if (error || response.statusCode !== 200) {
                debug(error);
                debug('Не удалось загрузить список языков');
            } else {
                try {
                    this.langs = JSON.parse(body).langs;
                    debug(this.langs);
                } catch (e) {
                    debug(e);
                }
            }
        });
    }

    getHelpText() {
        let message = ['Возможные варианты:'];
        for (let langId in this.langs) {
            message.push(langId + ' - ' + this.langs[langId]);
        }
        return message.join('\n');
    }

    translate(from, to, text) {
        return new Promise((resolve, reject) => {
            request({
                uri: TRANSLATE_URL,
                qs: {
                    key: this.apiKey,
                    text: text.trim(),
                    lang: from + '-' + to
                }
            }, (error, response, body) => {
                if (error || response.statusCode !== 200) {
                    debug('Не удалось выполнить запрос: %s', response.statusCode);
                    reject();
                } else {
                    try {
                        let result = JSON.parse(body);
                        debug(result);
                        resolve(result.text[0]);
                    } catch (e) {
                        debug(e);
                        reject();
                    }
                }
            });
        });
    }
}

module.exports = Translate;
