const Command = require('../command');

class User extends Command {

    constructor() {
        super();

        this.name = 'user';
        this.usage = this.name;
        this.description = 'Показать идентификатор пользователя';
        this.hidden = true;
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        message.reply(`username: ${message.getUsername()}\nname: ${message.getName()}`);
    }
}

module.exports = User;
