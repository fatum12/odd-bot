const Command = require('../command');

class Help extends Command {

    static get NAME() {
        return 'help';
    }

    constructor() {
        super();

        this.name = this.constructor.NAME;
        this.usage = this.name;
        this.description = 'Показать справку по всем командам';
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        const handlers = this.bot.getHandlers();
        let text = [];
        const isAdmin = message.isAdminUser();

        for (const handler of Object.values(handlers)) {
            if (handler.onlyAdmin() && !isAdmin || handler.hidden) {
                continue;
            }
            const description = handler.getDescription();
            const usage = this.bot.formatCommandName(message, handler.getUsage())
            if (description.length) {
                text.push(`${usage} - ${description}`);
            } else {
                text.push(handler.getUsage());
            }
        }

        if (text.length) {
            text = text.join('\n');
            text += `\n\n${Command.NOT_REQUIRED} - необязательные параметры`;
            message.reply(text);
        }
    }
}

module.exports = Help;
