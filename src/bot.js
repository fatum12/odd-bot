const Message = require('./message');
const debug = require('debug')('bot');
const util = require('util');
const HelpCommand = require('./commands/help');

class Bot {

    constructor(connector, commands) {
        this.connector = connector;
        this.handlers = {};

        commands.forEach((command) => {
            command.register(this);
        });

        this.connector.on('message', (message) => {
            this.handleMessage(new Message(message, this));
        });
    }
    
    run() {
        this.connector.connect().then(() => {
            debug('OddBot is initialized now');
        });
    }

    addHandler(commandName, handler) {
        if (commandName in this.handlers) {
            throw new Error(`Command "${commandName}" already registered`);
        }
        this.handlers[commandName] = handler;
    }

    getHandlers() {
        return this.handlers;
    }

    /**
     * @param {Message} message
     */
    handleMessage(message) {
        debug(message.resource);

        // определяем команду
        let text = message.getText().trim();
        let words = text.split(/\s+/);
        if (text === '' || words.length < 1) {
            return;
        }
        let commandName = words.shift().toLowerCase().replace(/^[/!]/, '');
        message.setText(words.join(' '));
        if (
            (commandName in this.handlers) &&
            (!this.handlers[commandName].onlyAdmin() || message.isAdminUser())
        ) {
            this.handlers[commandName].handleMessage(message);
        } else {
            this.sendMessage(
                message.getConversationId(),
                `Неизвестная команда: ${commandName}\n` +
                `Для получения списка доступных команд отправьте сообщение ${this.formatCommandName(message, HelpCommand.NAME)}`
            );
        }
    }

    sendMessage(conversationId, message /*, ... */) {
        let formatArgs = Array.prototype.slice.call(arguments, 1),
            text = util.format.apply(util, formatArgs);
        debug(text);
        this.connector.sendMessage(conversationId, text);
    }

    /**
     * @param {Message} message
     * @param {string} cmd
     */
    formatCommandName(message, cmd) {
        switch (message.getSource()) {
            case 'telegram':
                return '/' + cmd;
        }
        return cmd;
    }
}

module.exports = Bot;
