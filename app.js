const Bot = require('./src/bot.js');
const config = require('./config');
const debug = require('debug');

// log to stdout
debug.log = console.log.bind(console);

// команды
const Ping = require('./src/commands/ping');
const Weather = require('./src/commands/weather');
const Help = require('./src/commands/help');
const Currency = require('./src/commands/currency');
const DateTime = require('./src/commands/dateTime');
const Anekdot = require('./src/commands/anekdot');
const Bash = require('./src/commands/bash');
const Image = require('./src/commands/image');
const Gif = require('./src/commands/gif');
const Whois = require('./src/commands/whois');
const Translate = require('./src/commands/translate');
const Advice = require('./src/commands/advice');
const Coin = require('./src/commands/coin');
const Meduza = require('./src/commands/meduza');
const Password = require('./src/commands/password');
const Pikabu = require('./src/commands/pikabu');
const Countdown = require('./src/commands/countdown');
const StopAll = require('./src/commands/stopAll');
const StartAll = require('./src/commands/startAll');
const Calc = require('./src/commands/calc');
const ClearCache = require('./src/commands/admin/clearcache');
const Stat = require('./src/commands/admin/stat');
const Google = require('./src/commands/google');
const YouTube = require('./src/commands/youtube');
const Cities = require('./src/commands/cities');
const Cron = require('./src/commands/admin/cron');
const User = require('./src/commands/user');

const connectorName = config.get('connector');
const ConnectorClass = require(`./src/connectors/${connectorName}`);
const connector = new ConnectorClass(config.get(connectorName));

new Bot(connector, [
    new ClearCache(),
    new Stat(),
    new Cron(),
    new Help(),
    new Ping(),
    new Weather(config.get('openweathermap:apiKey')),
    new Currency(),
    new DateTime(),
    new Anekdot(),
    new Bash(),
    new Image(),
    new Gif(),
    new Whois(),
    new Translate(config.get('yandex:translatorApiKey')),
    new Advice(),
    new Coin(),
    new Meduza(),
    new Password(),
    new Pikabu(),
    new Countdown(),
    new StartAll(),
    new StopAll(),
    new Calc(),
    new Google(),
    new YouTube(),
    new Cities(),
    new User()
]).run();
